﻿USE teoria1;

-- 1 el numero de coches que ha alquilado el usuario 1
SELECT COUNT( DISTINCT a.coche) ncoches FROM alquileres a WHERE a.usuario=1;
-- numero de alquileres por usuario
SELECT COUNT(*) FROM alquileres a WHERE a.usuario=1; 

-- 2 numero de alquileres por mes
  SELECT MONTH( a.fecha) mes, COUNT(*) numero FROM alquileres a GROUP BY MONTH( a.fecha);
  
-- 3 numero de hombres y de mujeres
  SELECT COUNT(*) numero, u.sexo FROM usuarios u GROUP BY u.sexo; 
  
-- 4 el numero de alquileres de coches por color
  SELECT COUNT(*) nalquileres, c.color FROM coches c JOIN alquileres a ON c.codigoCoche = a.coche GROUP BY c.color;
  
-- 5 el numero de marcas de coches
  SELECT COUNT(DISTINCT c.marca) FROM coches c;
  
-- 6 numero de marcas de coches alquilados
  SELECT c.marca, COUNT(*) numero FROM coches c JOIN alquileres a ON c.codigoCoche = a.coche GROUP BY c.marca;             