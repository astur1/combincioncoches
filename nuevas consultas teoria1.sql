﻿USE teoria1;


 /* indicar el nombre de las marcas que se hayan vendido*/

  SELECT DISTINCT c.marca FROM alquileres a JOIN coches c ON a.coche = c.codigoCoche; 
  -- consulta optimizada
    -- subconsulta c1
    SELECT a.coche FROM alquileres a;
    SELECT DISTINCT c.marca FROM ( SELECT a.coche FROM alquileres a) c1 JOIN coches c ON c.codigoCoche= c1.coche; 