﻿     USE teoria1;


 /* 1 indicar el nombre de las marcas que se hayan vendido*/

  SELECT DISTINCT c.marca FROM alquileres a JOIN coches c ON a.coche = c.codigoCoche; 
  -- consulta optimizada
    -- subconsulta c1
    SELECT a.coche FROM alquileres a;
    -- consulta final
    SELECT DISTINCT c.marca FROM ( SELECT a.coche FROM alquileres a) c1 JOIN coches c ON c.codigoCoche= c1.coche;

/* 2 indicar los nombre de los usuarios que hayan alquilado alguna vez coches*/
  SELECT DISTINCT u.nombre FROM usuarios u JOIN alquileres a ON u.codigoUsuario = a.usuario;
  /* optimizada*/
    SELECT DISTINCT a.usuario FROM alquileres a;
    SELECT u.nombre FROM(SELECT DISTINCT a.usuario FROM alquileres a) c1 JOIN usuarios u ON c1.usuario= u.codigoUsuario ; 

      
/* 3 indicar los coches que no han sido alquilados*/
  CREATE OR REPLACE VIEW consulta3 AS
  SELECT c.codigoCoche FROM coches c LEFT JOIN alquileres a ON c.codigoCoche = a.coche WHERE a.coche IS NULL ;
  -- consulta optimizada
CREATE OR REPLACE VIEW coche_no AS
 SELECT DISTINCT a.coche FROM alquileres a;

CREATE OR REPLACE VIEW alqui_no AS
SELECT c.codigoCoche FROM coches c LEFT JOIN coche_no cn ON c.codigoCoche= cn.coche WHERE cn.coche IS NULL;

CREATE OR REPLACE VIEW consulta3op AS SELECT c.codigoCoche FROM coches c LEFT JOIN coche_no c1  
ON c.codigoCoche= c1.coche WHERE c1.coche IS NULL;

SELECT c.codigoCoche FROM coches c LEFT JOIN (SELECT DISTINCT a.coche FROM alquileres a) c1 ON c.codigoCoche= c1.coche WHERE c1.coche IS NULL;               
SELECT * FROM consulta3op c;

/* 4 usuarios que no han alquilado*/
CREATE OR REPLACE VIEW usu_no AS
SELECT u.codigoUsuario FROM usuarios u LEFT JOIN alquileres a ON u.codigoUsuario = a.usuario WHERE a.codigoAlquiler IS NULL;

/* consulta optimizada*/
CREATE OR REPLACE VIEW usu_alqui AS
SELECT a.usuario FROM alquileres a;

SELECT u.codigoUsuario FROM usuarios u LEFT JOIN usu_alqui ua ON u.codigoUsuario= ua.usuario WHERE ua.usuario IS NULL;
       
   
     