﻿USE teoria1;

--  indicar el nombre de las marcas  que se hayan alquilado coches
  -- crear la vista
   CREATE OR REPLACE VIEW consulta1 AS
 SELECT DISTINCT c.marca FROM alquileres a JOIN coches c ON a.coche = c.codigoCoche;

  -- ejecutar la vista
 SELECT * FROM consulta1 c;

-- para borrar la vista por si te has equivocado se hace asi
--  DROP VIEW y el nombre de la consulta a borrar acabado en ; 

-- esta vista da error porque no acepta una subconsulta
CREATE OR REPLACE VIEW c1consulta1 AS
SELECT a.coche FROM alquileres a;

CREATE OR REPLACE VIEW consulta1 AS 
 SELECT DISTINCT c.marca FROM c1consulta1 c1 JOIN coches c ON c.codigoCoche= c1.coche;
-- entonces para que no de error se pone así
  -- c1 consulta1: coches alquilados

  -- nombres de los usuarios que hayan alquilado alguna vez coches
  SELECT DISTINCT u.nombre FROM usuarios u JOIN alquileres a ON u.codigoUsuario = a.usuario; 

  CREATE OR REPLACE VIEW consulta2 AS
  SELECT DISTINCT u.nombre FROM usuarios u JOIN alquileres a ON u.codigoUsuario = a.usuario; 


